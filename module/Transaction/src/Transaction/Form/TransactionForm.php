<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TransactionForm
 *
 * @author mauro_000
 */
namespace Transaction\Form;

use Zend\Form\Form;

class TransactionForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'json_message',
            'type' => 'Textarea',
            'attributes' => array(
                'class' => 'add-transaction'
            ),
            'options' => array(
                'label' => 'JSON Message',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Send',
                'id' => 'submitbutton',
                'class' => 'add-transaction'
            ),
        ));
    }
}