<?php
namespace Transaction\Validation;

use Zend\Validator\AbstractValidator;
use Seld\JsonLint\JsonParser;

/**
 * Description of ValidateTransaction
 *
 * @author mauro_000
 */
class ValidateTransaction extends AbstractValidator {

    const INVALID_JSON = 'invalid_json';


    protected $messageTemplates;

    public function isValid($value) {
        $this->setValue($value);
        $parser = new JsonParser();
        $exception = $parser->lint($value);
        
        if ($exception) {
            $this->messageTemplates = array(
                self::INVALID_JSON => $exception->getMessage()
            );
            $this->setMessageTemplates();
            $this->error(self::INVALID_JSON);
            return false;
        }
        
        return true;
    }
    
    public function setMessageTemplates() {
        $this->abstractOptions['messageTemplates'] = $this->messageTemplates;
    }
}
