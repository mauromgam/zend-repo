<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Transaction\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Transaction\Model\Transaction;
use Transaction\Form\TransactionForm;
use HighRoller\PieChart;
use HighRoller\SeriesData;

class TransactionController extends AbstractActionController
{
    private $transactionTable;

    /**
     * List of transactions
     */
    public function indexAction()
    {
        // grab the paginator from the TransactionTable
        $paginator = $this->getTransactionTable()->fetchAll(true);
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page
        $paginator->setItemCountPerPage(20);

        return new ViewModel(array(
            'paginator' => $paginator
        ));
    }

    /**
    *
    * Action to consume the JSON Message
    * via AJAX/POST
    *
    * @return array
    *
    */
    public function postAddTransactionAction()
    {
        $return = false;
        $request = $this->getRequest();
        $transactionData = $request->getPost('data');
        if (isset($transactionData)) {
            $transaction = new Transaction();
            $transaction_array = $transaction->getDataFromJSON($transactionData);

            if (!$transaction->error) {
                $transaction->exchangeArray($transaction_array);
                if (isset($transaction->transactionArray)) {
                    $return = $this->getTransactionTable()->saveTransactionArray($transaction);
                } else {
                    $return = $this->getTransactionTable()->saveTransaction($transaction);
                }
            }
        }

        $jsonArray = array(
            'success' => ($return ? true : false),
        );

        $viewModel = new ViewModel();
        $viewModel->setTemplate('');
        $viewModel->setTerminal(true);

        return $viewModel->setVariables(
            array(
                'return' => \Zend\Json\Json::encode($jsonArray)
            )
        );
    }

    /**
    *
    * Action to render the add view
    * to create new transactions through a json message
    *
    * @return array
    *
    */
    public function addAction()
    {
        $form = new TransactionForm('add_transaction_form');
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $transaction = new Transaction();
            $form->setInputFilter($transaction->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $transaction_array = $transaction->getDataFromJSON($form->get('json_message')->getValue());

                if (!$transaction->error) {
                    $transaction->exchangeArray($transaction_array);
                    if (isset($transaction->transactionArray)) {
                        $this->getTransactionTable()->saveTransactionArray($transaction);
                    } else {
                        $this->getTransactionTable()->saveTransaction($transaction);
                    }

                    // Redirect to list of transactions
                    return $this->redirect()->toRoute('transaction');
                } else {
                    $form->get('json_message')->setMessages(
                            array("error" => $transaction->error)
                    );
                }
            }
        }
        return array('form' => $form);
    }

    /**
    *
    * Prints a graph of all solds by currency pair
    *
    * @return void
    *
    */
    public function saleGraphAction()
    {
        $sells = $this->getTransactionTable()->getAmountByCurrencyPair('amountSell');

        $piechart = new PieChart();
        $piechart->title->text = 'Amount Sold by Currency Pair';
        $series = new SeriesData();
        $series->name = 'Amount sold';

        $chartData = array();
        foreach ($sells as $sell) {
            $chartData[] = array(
                'name'=>$sell['currencyFrom']." to ".$sell['currencyTo'],
                'y'=>(int)$sell['amountSell']
            );
        }

        foreach ($chartData as $value) {
            $series->addData($value);
        }

        $piechart->addSeries($series);

        return new ViewModel(
            array(
                'highroller' => $piechart
            )
        );
    }

    /**
    *
    * Prints a graph of all boughts by currency pair
    *
    * @return void
    *
    */
    public function buyGraphAction()
    {
        $sells = $this->getTransactionTable()->getAmountByCurrencyPair('amountBuy');

        $piechart = new PieChart();
        $piechart->title->text = 'Amount Bought by Currency Pair';
        $series = new SeriesData();
        $series->name = 'Amount bought';

        $chartData = array();
        foreach ($sells as $sell) {
            $chartData[] = array(
                'name'=>$sell['currencyFrom']." to ".$sell['currencyTo'],
                'y'=>(float)$sell['amountBuy']
            );
        }

        foreach ($chartData as $value) {
            $series->addData($value);
        }

        $piechart->addSeries($series);

        return new ViewModel(
            array(
                'highroller' => $piechart
            )
        );
    }

    public function getTransactionTable()
    {
        if (!$this->transactionTable) {
            $sm = $this->getServiceLocator();
            $this->transactionTable = $sm->get('Transaction\Model\TransactionTable');
        }
        return $this->transactionTable;
    }
}
