<?php
namespace Transaction\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\Json\Json;
use Zend\Json\Exception;
use Transaction\Validation\ValidateTransaction;

class Transaction
{
    public $id;
    public $userId;
    public $currencyFrom;
    public $currencyTo;
    public $amountSell;
    public $amountBuy;
    public $rate;
    public $timePlaced;
    public $originatingCountry;
    public $transactionArray;
    public $error;
    protected $inputFilter; 

    const MAX_TRANSACTIONS_AT_ONCE = 100;
    
    public function exchangeArray($data)
    {
        if (isset($data['transactions'])) {
            $this->setTransactionArray($data['transactions']);
        } else {
            $this->setTransaction($data);
        }
    }
    
    public function setTransaction($data)
    {
        $this->id                 = (!empty($data['id'])) ? $data['id'] : null;
        $this->userId             = (!empty($data['userId'])) ? $data['userId'] : null;
        $this->currencyFrom       = (!empty($data['currencyFrom'])) ? $data['currencyFrom'] : null;
        $this->currencyTo         = (!empty($data['currencyTo'])) ? $data['currencyTo'] : null;
        $this->amountSell         = (!empty($data['amountSell'])) ? $data['amountSell'] : null;
        $this->amountBuy          = (!empty($data['amountBuy'])) ? $data['amountBuy'] : null;
        $this->rate               = (!empty($data['rate'])) ? $data['rate'] : null;
        $this->timePlaced         = (!empty($data['timePlaced'])) ? $data['timePlaced'] : null;
        $this->originatingCountry = (!empty($data['originatingCountry'])) ? $data['originatingCountry'] : null;
    }
    
    public function setTransactionArray($transaction_array)
    {
        foreach ($transaction_array as $transaction) {
            $this->setTransaction($transaction);
            $this->transactionArray[] = array(
                'id' => $this->id,
                'userId' => $this->userId,
                'currencyFrom' => $this->currencyFrom,
                'currencyTo' => $this->currencyTo,
                'amountSell' => $this->amountSell,
                'amountBuy' => $this->amountBuy,
                'rate' => $this->rate,
                'timePlaced' => $this->timePlaced,
                'originatingCountry' => $this->originatingCountry
            );
        }
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    /**
    *
    * Action that renders the add view
    * to create new transactions through a json message
    *
    * @return array/boolean
    *
    */
    public function getDataFromJSON($json_string)
    {
        try {
            $transaction_array = Json::decode($json_string, Json::TYPE_ARRAY);
        } catch (Exception $e) {
            throw $e;
        }
        
        if ($this->isValid($transaction_array)) {
            return $transaction_array;
        }
        
        return false;
    }
    
    /**
    *
    * This method validates if the JSON object provided is valid.
    * It can contain only one currency transaction, in this case
    * the JSON string would be something like
    * {"userId": "134256", "currencyFrom": "EUR", "currencyTo": "GBP", "amountSell": 1000, "amountBuy": 747.10, "rate": 0.7471, "timePlaced" : "24-JAN-15 10:27:44", "originatingCountry" : "FR"}
    * or it can contain an array of transactions which should be something like
    * {"transactions":[{userId:123456, ..., "originatingCountry" : "FR"}, ..., {userId:999999, ..., "originatingCountry" : "BR"}]}
    * 
    *
    * @return boolean
    *
    */
    public function isValid($t_array)
    {
        $this->error = null;
        if ($this->isValidSingleTransaction($t_array)) {
            return true;
        } elseif ($this->isValidArrayOfTransactions($t_array)) {
            return true;
        }
        
        return false;
    }

    public function isValidSingleTransaction($t_array)
    {
        if (!isset($t_array['userId'],$t_array['currencyFrom'],$t_array['rate'],
                $t_array['currencyTo'], $t_array['amountSell'],$t_array['amountBuy'],
                $t_array['timePlaced'],$t_array['originatingCountry'])
        ) {
            if (!isset($t_array['transactions'])) {
                $this->error = "One or more transaction property is incorret.";
            }
            return false;
        }
        
        return true;
    }

    public function isValidArrayOfTransactions($t_array)
    {
        $return = true;
        
        if (isset($t_array['transactions']) && count($t_array['transactions']) <= self::MAX_TRANSACTIONS_AT_ONCE) {
            foreach ($t_array['transactions'] as $transaction) {
                if (!$this->isValidSingleTransaction($transaction)) {
                    $return = false;
                    break;
                }
            }
        } elseif (isset($t_array['transactions']) && count($t_array['transactions']) > self::MAX_TRANSACTIONS_AT_ONCE) {
            $this->error = "The maximum number of ".self::MAX_TRANSACTIONS_AT_ONCE.
                    " transactions at once was exceeded.";
            $return = false;
        }
        
        return $return;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'json_message',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 20000,
                        ),
                    ),
                    new ValidateTransaction()
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}