<?php
namespace Transaction\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Sql;

class TransactionTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated=false)
    {
        if ($paginated) {
            // create a new Select object for the table currency_transaction
            $select = new Select('transaction');
            $select->order('timePlaced ASC');
            // create a new result set based on the Transaction entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Transaction());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select(function (Select $select) {
           $select->order('timePlaced ASC');
        });
        return $resultSet;
    }

    public function getTransaction($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getAmountByCurrencyPair($amountType)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('transaction');
        $select->columns(array(
            'currencyFrom',
            'currencyTo',
            $amountType=>new Expression('SUM('.$amountType.')')));
        $select->group(array('currencyFrom', 'currencyTo'));
        $select->order('currencyFrom');

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            throw new \Exception("Could not find row $id");
        }
        return $result;
    }

    public function getTransactionData(Transaction $transaction) {
        return array(
           'id'                 => $transaction->id,
           'userId'             => $transaction->userId,
           'currencyFrom'       => $transaction->currencyFrom,
           'currencyTo'         => $transaction->currencyTo,
           'amountSell'         => $transaction->amountSell,
           'amountBuy'          => $transaction->amountBuy,
           'rate'               => $transaction->rate,
           'timePlaced'         => date("Y-m-d H:i:s", strtotime($transaction->timePlaced)),
           'originatingCountry' => $transaction->originatingCountry,
        );
    }

    public function saveTransaction(Transaction $transaction)
    {
        $data = $this->getTransactionData($transaction);

        $id = (int) $transaction->id;
        if ($id == 0) {
            return $this->tableGateway->insert($data);
        } elseif ($this->getTransaction($id)) {
            return $this->tableGateway->update($data, array('id' => $id));
        } else {
            throw new \Exception('Transaction id does not exist');
        }
    }

    public function saveTransactionArray(Transaction $transaction)
    {
        foreach ($transaction->transactionArray as $t) {
            $transaction->setTransaction($t);
            $data = $this->getTransactionData($transaction);

            $id = (int) $t['id'];
            if ($id == 0) {
                $return = $this->tableGateway->insert($data);
            } elseif ($this->getTransaction($id)) {
                $return = $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Transaction id does not exist');
            }

            if (!$return) {
                throw new \Exception('ERROR: Transaction with data: '.  var_export($data) .' contain errors.');
            }
        }
        return $return;
    }

    public function deleteTransaction($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}