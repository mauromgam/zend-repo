/* 
 * Customized JS
 */

$(document).ready(function(){

   $('.phone_us').mask('(000) 000-0000', {placeholder: "(000) 000-0000", clearIfNotMatch: true});
   $('.date').mask('00/00/0000', {placeholder: "__/__/____", clearIfNotMatch: true});
   
});
