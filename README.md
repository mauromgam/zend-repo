Transactions Application
=======================

Introduction
------------
This application processes currency transactions. The transactions are provided by a message in JSON format and are validated and inserted in the database.

Instructions of use
---------------------------

There are two ways of saving transactions in the database.

1. Use the New Transaction page and insert the transaction JSON text into the textarea.


2. Send the JSON text through POST to the service http://domain/transaction/post-add-transaction. It will return a JSON array with the result like this {'success' : true}|{'success' : false}.


### JSON message format

The JSON message format is accepted in two formats only. The array must have this fields, otherwise it will fail.


1. A JSON array with the transaction data like the example below.

    {
        "userId": "917",
        "currencyFrom": "USD",
        "currencyTo": "GBP",
        "amountSell": 647,
        "amountBuy": 711.32,
        "rate": 1.0994,
        "timePlaced" : "20-Jan-1 20:36:59",
        "originatingCountry" : "PT"
    }

2. An array of transactions as follow.

    data = {

        "transactions":[

            {

                "userId": "917",

                "currencyFrom": "USD",

                "currencyTo": "GBP",

                "amountSell": 647,

                "amountBuy": 711.32,

                "rate": 1.0994,

                "timePlaced" : "20-Jan-1 20:36:59",

                "originatingCountry" : "PT"

            },

            {
                "userId": "20",

                "currencyFrom": "AUD",

                "currencyTo": "BRL",

                "amountSell": 813,

                "amountBuy": 563.64,

                "rate": 0.6933,

                "timePlaced" : "1-Jul-4 17:47:38",

                "originatingCountry" : "MT"

            }

        ]

    }

    - `data` is the name of the POST variable.

Just for knowledge, the data provided is totally random.


### Functionalities implemented

1. Validation and insertion of JSON message data into the database.

2. List of transactions.

3. Graphs of transactions by currency pair.

4. API which saves the JSON message data received through a POST request into the database.

5. Page with a form to save transactions provided by a JSON string too.

6. Insertion of transactions at once limited to 100. Can be changed in \Transaction\Model\Transaction.php by setting the constant MAX_TRANSACTIONS_AT_ONCE to other value.

### Third party modules

    - seld/jsonlint

    - jfloff/highroller-zf2
